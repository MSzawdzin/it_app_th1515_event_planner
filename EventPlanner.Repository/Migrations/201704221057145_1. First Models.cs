namespace EventPlanner.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1FirstModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        PhoneNumber = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Reservations",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        When = c.DateTime(nullable: false),
                        Duration = c.Time(nullable: false, precision: 7),
                        NoOfGuests = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        Hall_ID = c.Int(nullable: false),
                        HallSetting_ID = c.Int(nullable: false),
                        Client_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Halls", t => t.Hall_ID, cascadeDelete: true)
                .ForeignKey("dbo.HallSettings", t => t.HallSetting_ID, cascadeDelete: true)
                .ForeignKey("dbo.Clients", t => t.Client_ID, cascadeDelete: true)
                .Index(t => t.Hall_ID)
                .Index(t => t.HallSetting_ID)
                .Index(t => t.Client_ID);
            
            CreateTable(
                "dbo.Caterings",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Reservations", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.Dishes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DishType = c.Int(nullable: false),
                        WhenToServe = c.DateTime(nullable: false),
                        NumberOfGuests = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        Catering_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Caterings", t => t.Catering_ID, cascadeDelete: true)
                .Index(t => t.Catering_ID);
            
            CreateTable(
                "dbo.Meals",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Prize = c.Double(nullable: false),
                        DishType = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Halls",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Capacity = c.Int(nullable: false),
                        AdditionalInfo = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.HallSettings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NumberOfSeats = c.Int(nullable: false),
                        TablesSetting = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DishMeal",
                c => new
                    {
                        DishID = c.Int(nullable: false),
                        MealID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DishID, t.MealID })
                .ForeignKey("dbo.Dishes", t => t.DishID, cascadeDelete: true)
                .ForeignKey("dbo.Meals", t => t.MealID, cascadeDelete: true)
                .Index(t => t.DishID)
                .Index(t => t.MealID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reservations", "Client_ID", "dbo.Clients");
            DropForeignKey("dbo.Reservations", "HallSetting_ID", "dbo.HallSettings");
            DropForeignKey("dbo.Reservations", "Hall_ID", "dbo.Halls");
            DropForeignKey("dbo.Caterings", "ID", "dbo.Reservations");
            DropForeignKey("dbo.DishMeal", "MealID", "dbo.Meals");
            DropForeignKey("dbo.DishMeal", "DishID", "dbo.Dishes");
            DropForeignKey("dbo.Dishes", "Catering_ID", "dbo.Caterings");
            DropIndex("dbo.DishMeal", new[] { "MealID" });
            DropIndex("dbo.DishMeal", new[] { "DishID" });
            DropIndex("dbo.Dishes", new[] { "Catering_ID" });
            DropIndex("dbo.Caterings", new[] { "ID" });
            DropIndex("dbo.Reservations", new[] { "Client_ID" });
            DropIndex("dbo.Reservations", new[] { "HallSetting_ID" });
            DropIndex("dbo.Reservations", new[] { "Hall_ID" });
            DropTable("dbo.DishMeal");
            DropTable("dbo.HallSettings");
            DropTable("dbo.Halls");
            DropTable("dbo.Meals");
            DropTable("dbo.Dishes");
            DropTable("dbo.Caterings");
            DropTable("dbo.Reservations");
            DropTable("dbo.Clients");
        }
    }
}
