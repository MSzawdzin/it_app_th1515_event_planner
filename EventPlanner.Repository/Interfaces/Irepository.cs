﻿using EventPlanner.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EventPlanner.Repository.Interfaces
{
    public interface IRepository<T> where T : class, IDBEntity
    {
        int Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        T FindById(int id, params Expression<Func<T, object>>[] includeItems);
        List<T> Find(Func<T, bool> filter, params Expression<Func<T, object>>[] includeItems);
        bool Any(Func<T, bool> filter, params Expression<Func<T, object>>[] includeItems);
        List<T> FindAll(params Expression<Func<T, object>>[] includeItems);
    }
}
