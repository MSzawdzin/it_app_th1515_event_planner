﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using EventPlanner.Models.ModelsDTO;
using System.Data.Entity.ModelConfiguration.Conventions;
//using EventPlanner.Models.ModelsDTO;

namespace EventPlanner.Repository
{
    class EventPlannerDBContext : DbContext
    {
        public EventPlannerDBContext() : base("EventPlannerConnectionString")
        {
            Database.CreateIfNotExists();
            base.Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer<EventPlannerDBContext>(null);
        }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {


            modelBuilder.Entity<ClientDTO>().ToTable("Clients");
            modelBuilder.Entity<CateringDTO>().ToTable("Caterings");
            modelBuilder.Entity<DishDTO>().ToTable("Dishes");
            modelBuilder.Entity<HallDTO>().ToTable("Halls");
            modelBuilder.Entity<HallSettingDTO>().ToTable("HallSettings");
            modelBuilder.Entity<MealDTO>().ToTable("Meals");
            modelBuilder.Entity<ReservationDTO>().ToTable("Reservations");

            modelBuilder.Entity<CateringDTO>()
                .HasRequired(s => s.Reservation)
                .WithOptional(c => c.Catering);

            modelBuilder.Entity<HallDTO>().HasMany(s => s.Reservations).WithRequired(s => s.Hall);
            modelBuilder.Entity<HallSettingDTO>().HasMany(s => s.Reservations).WithRequired(s => s.HallSetting);
            modelBuilder.Entity<ClientDTO>().HasMany(s => s.Reservations).WithRequired(s => s.Client);


            modelBuilder.Entity<DishDTO>()
                .HasRequired(s => s.Catering)
                .WithMany(s => s.Dishes);
            modelBuilder.Entity<DishDTO>()
                .HasMany(s => s.Meals)
                .WithMany(c => c.Dishes)
                .Map(cs =>
                {
                    cs.MapLeftKey("DishID");
                    cs.MapRightKey("MealID");
                    cs.ToTable("DishMeal");
                });


        }
    }
}
