﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventPlanner.Repository.Interfaces;
using EventPlanner.Models.Interfaces;
using System.Linq.Expressions;
using System.Data.Entity;

namespace EventPlanner.Repository
{
    public class Repository<T> : IRepository<T> where T : class, IDBEntity
    {
        public int Add(T entity)
        {
            using (var context = new EventPlannerDBContext())
            {
                context.Set<T>().Add(entity);
                context.SaveChanges();
                context.Entry(entity).GetDatabaseValues();
                return entity.ID;
            }
        }

        public void Update(T entity)
        {
            using (var context = new EventPlannerDBContext())
            {
                context.Set<T>().Attach(entity);
                context.Entry<T>(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Delete(T entity)
        {
            using (var context = new EventPlannerDBContext())
            {
                entity.IsDeleted = true;
                context.Set<T>().Attach(entity);
                context.Entry<T>(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public T FindById(int id, params Expression<Func<T, object>>[] includeItems)
        {
            using (var context = new EventPlannerDBContext())
            {
                var result = context.Set<T>() as IQueryable<T>;
                foreach (var include in includeItems)
                {
                    result = result.Include(include);
                }
                return result.FirstOrDefault(x => x.ID == id && !x.IsDeleted);
            }
        }

        public List<T> Find(Func<T, bool> filter, params Expression<Func<T, object>>[] includeItems)
        {
            using (var context = new EventPlannerDBContext())
            {
                var result = context.Set<T>() as IQueryable<T>;
                foreach (var include in includeItems)
                {
                    result = result.Include(include);
                }
                return result.Where(x => !x.IsDeleted).Where(filter).ToList();
            }
        }

        public bool Any(Func<T, bool> filter, params Expression<Func<T, object>>[] includeItems)
        {
            using (var context = new EventPlannerDBContext())
            {
                var result = context.Set<T>() as IQueryable<T>;
                foreach (var include in includeItems)
                {
                    result = result.Include(include);
                }
                return result.Where(x => !x.IsDeleted).Where(filter).Any();
            }
        }

        public List<T> FindAll(params Expression<Func<T, object>>[] includeItems)
        {
            using (var context = new EventPlannerDBContext())
            {
                var result = context.Set<T>() as IQueryable<T>;
                foreach (var include in includeItems)
                {
                    result = result.Include(include);
                }
                return result.Where(x => !x.IsDeleted).ToList();
            }
        }


    }
}
