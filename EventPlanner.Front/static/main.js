'use strict';

$(function () {
  $('.-fetch').each(function () {
    var $item = $(this);
    var $loading = $item.find('.-loading');
    var uri = $item.data('uri');

    $item.append(
      $('<h2/>').text(uri),
      $loading
    );

    setTimeout(function () {
      $.get(uri, function (response) {
        $loading.remove();
        $item.append(
          $('<pre />').text(
            JSON.stringify(response, null, 2)
          )
        );
      });
    });
  });
});