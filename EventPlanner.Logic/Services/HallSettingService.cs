﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventPlanner.Logic.Interfaces;
using EventPlanner.Models.Models;
using EventPlanner.Models.ModelsDTO;
using EventPlanner.Repository.Interfaces;
using EventPlanner.Repository;

namespace EventPlanner.Logic.Services
{
    public class HallSettingService : IHallSettingService
    {
        private IRepository<HallSettingDTO> HallSettingRepo { get; set; }

        public HallSettingService()
        {
            HallSettingRepo = new Repository<HallSettingDTO>();
        }

        public HallSettingService(IRepository<HallSettingDTO> repo)
        {
            HallSettingRepo = repo;
        }


        public void AddHallSetting(HallSettingModel hallSetting)
        {
            HallSettingDTO HallSettingDTO = Models.AutoMapper.Map<HallSettingModel, HallSettingDTO>(hallSetting);
            HallSettingRepo.Add(HallSettingDTO);
        }

        public int AddHallSettingAndReturnId(HallSettingModel hallSetting)
        {
            HallSettingDTO HallSettingDTO = Models.AutoMapper.Map<HallSettingModel, HallSettingDTO>(hallSetting);
            return HallSettingRepo.Add(HallSettingDTO);
        }

        public void DeleteHallSetting(HallSettingModel hallSetting)
        {
            HallSettingDTO HallSettingDTO = Models.AutoMapper.Map<HallSettingModel, HallSettingDTO>(hallSetting);
            HallSettingRepo.Delete(HallSettingDTO);
        }

        public void DeleteHallSettingById(int hallSettingId)
        {
            var HallSettingDTO = HallSettingRepo.FindById(hallSettingId);
            HallSettingRepo.Delete(HallSettingDTO);
        }

        public void EditHallSetting(HallSettingModel hallSetting)
        {
            HallSettingDTO HallSettingDTO = Models.AutoMapper.Map<HallSettingModel, HallSettingDTO>(hallSetting);
            HallSettingRepo.Update(HallSettingDTO);
        }

        public List<HallSettingModel> GetAllHallSettings()
        {
            List<HallSettingModel> HallSettinges = HallSettingRepo.FindAll(x => x.Reservations).Select(x => Models.AutoMapper.Map<HallSettingDTO, HallSettingModel>(x)).ToList();
            return HallSettinges;
        }

        public HallSettingModel GetHallSettingById(int hallSettingId)
        {
            HallSettingModel HallSetting = Models.AutoMapper.Map<HallSettingDTO, HallSettingModel>(HallSettingRepo.FindById(hallSettingId,  x => x.Reservations));
            return HallSetting;
        }
    }
}
