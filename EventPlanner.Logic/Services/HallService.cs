﻿using EventPlanner.Models.Models;
using EventPlanner.Models;
using EventPlanner.Models.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventPlanner.Logic.Interfaces;
using EventPlanner.Repository.Interfaces;
using EventPlanner.Repository;

namespace EventPlanner.Logic.Services
{
    public class HallService : IHallService
    {

        private IRepository<HallDTO> hallRepo { get; set; }

        public HallService()
        {
            hallRepo = new Repository<HallDTO>();
        }

        public HallService(IRepository<HallDTO> repo)
        {
            hallRepo = repo;
        }

        public void AddHall(HallModel Hall)
        {
            HallDTO HallDTO = Models.AutoMapper.Map<HallModel, HallDTO>(Hall);
            hallRepo.Add(HallDTO);
        }

        public int AddHallAndReturnId(HallModel Hall)
        {
            HallDTO HallDTO = Models.AutoMapper.Map<HallModel, HallDTO>(Hall);
            return hallRepo.Add(HallDTO);
        }

        public void DeleteHall(HallModel Hall)
        {
            HallDTO HallDTO = Models.AutoMapper.Map<HallModel, HallDTO>(Hall);

            hallRepo.Delete(HallDTO);
        }

        public void DeleteHallById(int HallId)
        {
            HallDTO HallDTO = hallRepo.FindById(HallId);
            hallRepo.Delete(HallDTO);

        }

        public void UpdateHallReservations(int HallId, ReservationModel reservationModel)
        {
            HallDTO HallDTO = hallRepo.FindById(HallId);
            ReservationDTO reservationDTO = Models.AutoMapper.Map<ReservationModel, ReservationDTO>(reservationModel);
            HallDTO.Reservations.Add(reservationDTO);
            hallRepo.Update(HallDTO);
        }

        public void EditHall(HallModel Hall)
        {
            HallDTO HallDTO = Models.AutoMapper.Map<HallModel, HallDTO>(Hall);

            hallRepo.Update(HallDTO);
        }

        public List<HallModel> GetAllHalls()
        {
            List<HallModel> Halls = hallRepo.FindAll(x => x.Reservations).Select(x => Models.AutoMapper.Map<HallDTO, HallModel>(x)).ToList();

            return Halls;
        }

        public HallModel GetHallById(int HallId)
        {
            HallModel Hall = Models.AutoMapper.Map<HallDTO, HallModel>(hallRepo.FindById(HallId, x => x.Reservations));

            return Hall;
        }

        public List<HallModel> GetHallByIdOfReservation(int reservationId)
        {
            List<HallModel> hallModel = hallRepo.Find(x => x.Reservations.Any(cx => cx.ID == reservationId)).Select(x => Models.AutoMapper.Map<HallDTO, HallModel>(x)).ToList();
            //Hall.Desks = Hall.Desks.Where(x => x.Status != Statuses.Unassigned && x.IsDeleted == false).ToList();
            return hallModel;
        }
    }
}
