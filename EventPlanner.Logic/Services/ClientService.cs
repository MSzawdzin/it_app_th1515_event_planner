﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventPlanner.Models.Models;
using EventPlanner.Models.ModelsDTO;
using EventPlanner.Repository;
using EventPlanner.Repository.Interfaces;

namespace EventPlanner.Logic.Services
{
    public class ClientService
    {
        private IRepository<ClientDTO> ClientRepo { get; set; }

        public ClientService()
        {
            ClientRepo = new Repository<ClientDTO>();
        }

        public ClientService(IRepository<ClientDTO> repo)
        {
            ClientRepo = repo;
        }

        public void AddClient(ClientModel client)
        {
            ClientDTO ClientDTO = Models.AutoMapper.Map<ClientModel, ClientDTO>(client);
            ClientRepo.Add(ClientDTO);
        }

        public void DeleteClient(ClientModel client)
        {
            ClientDTO ClientDTO = Models.AutoMapper.Map<ClientModel, ClientDTO>(client);

            ClientRepo.Delete(ClientDTO);
        }

        public void DeleteClientById(int clientId)
        {
            ClientDTO ClientDTO = ClientRepo.FindById(clientId);
            ClientRepo.Delete(ClientDTO);
        }

        public void EditClient(ClientModel client)
        {
            ClientDTO ClientDTO = Models.AutoMapper.Map<ClientModel, ClientDTO>(client);

            ClientRepo.Update(ClientDTO);
        }

        public List<ClientModel> GetAllClients()
        {
            List<ClientModel> Clients = ClientRepo.FindAll(x => x.Reservations).Select(x => Models.AutoMapper.Map<ClientDTO, ClientModel>(x)).ToList();

            return Clients;
        }

        public ClientModel GetClientById(int clientId)
        {
            ClientModel Client = Models.AutoMapper.Map<ClientDTO, ClientModel>(ClientRepo.FindById(clientId, x => x.Reservations));

            return Client;
        }
    }
}
