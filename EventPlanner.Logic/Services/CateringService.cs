﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventPlanner.Models.Models;
using EventPlanner.Models.ModelsDTO;
using EventPlanner.Repository;
using EventPlanner.Repository.Interfaces;

namespace EventPlanner.Logic.Services
{
    public class CateringService
    {
        private IRepository<CateringDTO> CateringRepo { get; set; }

        public CateringService()
        {
            CateringRepo = new Repository<CateringDTO>();
        }

        public CateringService(IRepository<CateringDTO> repo)
        {
            CateringRepo = repo;
        }

        public void AddCatering(CateringModel catering)
        {
            CateringDTO cateringDTO = Models.AutoMapper.Map<CateringModel, CateringDTO>(catering);
            CateringRepo.Add(cateringDTO);
        }

        public void DeleteCatering(CateringModel catering)
        {
            CateringDTO cateringDTO = Models.AutoMapper.Map<CateringModel, CateringDTO>(catering);

            CateringRepo.Delete(cateringDTO);
        }

        public void DeleteCateringById(int cateringId)
        {
            CateringDTO cateringDTO = CateringRepo.FindById(cateringId);
            CateringRepo.Delete(cateringDTO);
        }

        public void EditCatering(CateringModel catering)
        {
            CateringDTO cateringDTO = Models.AutoMapper.Map<CateringModel, CateringDTO>(catering);

            CateringRepo.Update(cateringDTO);
        }

        public List<CateringModel> GetAllCaterings()
        {
            List<CateringModel> caterings = CateringRepo.FindAll(x => x.Dishes, x=>x.Reservation).Select(x => Models.AutoMapper.Map<CateringDTO, CateringModel>(x)).ToList();

            return caterings;
        }

        public CateringModel GetCateringById(int cateringId)
        {
            CateringModel catering = Models.AutoMapper.Map<CateringDTO, CateringModel>(CateringRepo.FindById(cateringId, x => x.Dishes, x => x.Reservation));

            return catering;
        }
    }
}
