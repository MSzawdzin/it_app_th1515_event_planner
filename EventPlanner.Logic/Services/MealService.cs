﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventPlanner.Models.Models;
using EventPlanner.Models.ModelsDTO;
using EventPlanner.Repository;
using EventPlanner.Repository.Interfaces;

namespace EventPlanner.Logic.Services
{
    public class MealService
    {
        private IRepository<MealDTO> mealRepo { get; set; }

        public MealService()
        {
            mealRepo = new Repository<MealDTO>();
        }

        public MealService(IRepository<MealDTO> repo)
        {
            mealRepo = repo;
        }

        public void AddMeal(MealModel meal)
        {
            MealDTO MealDTO = Models.AutoMapper.Map<MealModel, MealDTO>(meal);
            mealRepo.Add(MealDTO);
        }

        public void DeleteMeal(MealModel meal)
        {
            MealDTO MealDTO = Models.AutoMapper.Map<MealModel, MealDTO>(meal);

            mealRepo.Delete(MealDTO);
        }

        public void DeleteMealById(int mealId)
        {
            MealDTO MealDTO = mealRepo.FindById(mealId);
            mealRepo.Delete(MealDTO);
        }

        public void EditMeal(MealModel meal)
        {
            MealDTO MealDTO = Models.AutoMapper.Map<MealModel, MealDTO>(meal);

            mealRepo.Update(MealDTO);
        }

        public List<MealModel> GetAllMeals()
        {
            List<MealModel> meals = mealRepo.FindAll(x => x.Dishes).Select(x => Models.AutoMapper.Map<MealDTO, MealModel>(x)).ToList();

            return meals;
        }

        public MealModel GetMealById(int mealId)
        {
            MealModel meal = Models.AutoMapper.Map<MealDTO, MealModel>(mealRepo.FindById(mealId, x => x.Dishes));

            return meal;
        }
    }
}
