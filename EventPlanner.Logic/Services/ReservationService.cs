﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventPlanner.Models.Models;
using EventPlanner.Models.ModelsDTO;
using EventPlanner.Repository;
using EventPlanner.Repository.Interfaces;

namespace EventPlanner.Logic.Services
{
    public class ReservationService
    {
        private IRepository<ReservationDTO> ReservationRepo { get; set; }

        public ReservationService()
        {
            ReservationRepo = new Repository<ReservationDTO>();
        }

        public ReservationService(IRepository<ReservationDTO> repo)
        {
            ReservationRepo = repo;
        }

        public void AddReservation(ReservationModel reservation)
        {
            ReservationDTO reservationDTO = Models.AutoMapper.Map<ReservationModel, ReservationDTO>(reservation);
            ReservationRepo.Add(reservationDTO);
        }

        public void DeleteReservation(ReservationModel reservation)
        {
            ReservationDTO reservationDTO = Models.AutoMapper.Map<ReservationModel, ReservationDTO>(reservation);

            ReservationRepo.Delete(reservationDTO);
        }

        public void DeleteReservationById(int reservationId)
        {
            ReservationDTO reservationDTO = ReservationRepo.FindById(reservationId);
            ReservationRepo.Delete(reservationDTO);
        }

        public void EditReservation(ReservationModel reservation)
        {
            ReservationDTO reservationDTO = Models.AutoMapper.Map<ReservationModel, ReservationDTO>(reservation);

            ReservationRepo.Update(reservationDTO);
        }

        public List<ReservationModel> GetAllReservations()
        {
            List<ReservationModel> reservations = ReservationRepo.FindAll(x => x.Catering, x=>x.Client, x=>x.Hall, x=>x.HallSetting).Select(x => Models.AutoMapper.Map<ReservationDTO, ReservationModel>(x)).ToList();

            return reservations;
        }

        public ReservationModel GetReservationById(int reservationId)
        {
            ReservationModel reservation = Models.AutoMapper.Map<ReservationDTO, ReservationModel>(ReservationRepo.FindById(reservationId, x => x.Catering, x => x.Client, x => x.Hall, x => x.HallSetting));

            return reservation;
        }
    }
}
