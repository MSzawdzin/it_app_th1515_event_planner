﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventPlanner.Logic.Interfaces;
using EventPlanner.Models.Models;
using EventPlanner.Models.ModelsDTO;
using EventPlanner.Repository.Interfaces;
using EventPlanner.Repository;

namespace EventPlanner.Logic.Services
{
    public class DishService : IDishService
    {
        private IRepository<DishDTO> dishRepo { get; set; }

        public DishService()
        {
            dishRepo = new Repository<DishDTO>();
        }

        public DishService(IRepository<DishDTO> repo)
        {
            dishRepo = repo;
        }


        public void AddDish(DishModel dish)
        {
            DishDTO dishDTO = Models.AutoMapper.Map<DishModel, DishDTO>(dish);
            dishRepo.Add(dishDTO);
        }

        public void DeleteDish(DishModel dish)
        {
            DishDTO dishDTO = Models.AutoMapper.Map<DishModel, DishDTO>(dish);
            dishRepo.Delete(dishDTO);
        }

        public void DeleteDishById(int dishId)
        {
            var dishDTO = dishRepo.FindById(dishId);
            dishRepo.Delete(dishDTO);
        }

        public void EditDish(DishModel dish)
        {
            DishDTO dishDTO = Models.AutoMapper.Map<DishModel, DishDTO>(dish);
            dishRepo.Update(dishDTO);
        }
        
        public List<DishModel> GetAllDishes()
        {
            List<DishModel> dishes = dishRepo.FindAll(x => x.Catering, x=>x.Meals).Select(x => Models.AutoMapper.Map<DishDTO, DishModel>(x)).ToList();
            return dishes;
        }

        public DishModel GetDishById(int dishId)
        {
            DishModel dish = Models.AutoMapper.Map<DishDTO, DishModel>(dishRepo.FindById(dishId, x => x.ID, x => x.Meals));
            return dish;
        }
    }
}
