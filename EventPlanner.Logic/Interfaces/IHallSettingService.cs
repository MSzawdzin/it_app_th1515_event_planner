﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventPlanner.Models.Models;

namespace EventPlanner.Logic.Interfaces
{
    interface IHallSettingService
    {
        void AddHallSetting(HallSettingModel hallSetting);

        int AddHallSettingAndReturnId(HallSettingModel hallSetting);

        void DeleteHallSetting(HallSettingModel hallSetting);

        void DeleteHallSettingById(int hallSettingId);

        void EditHallSetting(HallSettingModel hallSetting);

        List<HallSettingModel> GetAllHallSettings();

        HallSettingModel GetHallSettingById(int hallSettingId);
    }
}
