﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventPlanner.Models.Models;

namespace EventPlanner.Logic.Interfaces
{
    interface IClientService
    {
        void AddClient(ClientModel client);

        void DeleteClient(ClientModel client);

        void DeleteClientById(int clientId);

        void EditClient(ClientModel client);

        List<ClientModel> GetAllClients();

        ClientModel GetClientById(int clientId);
    }
}
