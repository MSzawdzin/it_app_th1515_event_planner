﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventPlanner.Models.Models;

namespace EventPlanner.Logic.Interfaces
{
    interface IHallService
    {
        void AddHall(HallModel Hall);

        int AddHallAndReturnId(HallModel Hall);

        void DeleteHall(HallModel Hall);

        void DeleteHallById(int HallId);

        void UpdateHallReservations(int HallId, ReservationModel reservationModel);

        void EditHall(HallModel Hall);

        List<HallModel> GetAllHalls();

        HallModel GetHallById(int HallId);

        List<HallModel> GetHallByIdOfReservation(int reservationId);
    }
}
