﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventPlanner.Models.Models;

namespace EventPlanner.Logic.Interfaces
{
    interface IDishService
    {
        void AddDish(DishModel dish);

        void DeleteDish(DishModel dish);

        void DeleteDishById(int dishId);

        void EditDish(DishModel dish);

        List<DishModel> GetAllDishes();

        DishModel GetDishById(int dishId);
    }
}
