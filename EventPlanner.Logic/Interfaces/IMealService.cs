﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventPlanner.Models.Models;

namespace EventPlanner.Logic.Interfaces
{
    interface IMealService
    {
        void AddMeal(MealModel meal);

        void DeleteMeal(MealModel meal);

        void DeleteMealById(int mealId);

        void EditMeal(MealModel meal);

        List<MealModel> GetAllMeals();

        MealModel GetMealById(int mealId);
    }
}
