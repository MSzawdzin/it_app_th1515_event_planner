﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventPlanner.Models.Models;

namespace EventPlanner.Logic.Interfaces
{
    interface ICateringService
    {
        void AddCatering(CateringModel catering);

        void DeleteCatering(CateringModel catering);

        void DeleteCateringById(int cateringId);

        void EditCatering(CateringModel catering);

        List<CateringModel> GetAllCaterings();

        CateringModel GetCateringById(int cateringId);
    }
}
