﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EventPlanner.Logic.Services;
using EventPlanner.Models;
using EventPlanner.Models.Models;

namespace EventPlanner.Controllers
{
    public class DishesController : Controller
    {
        private DishService service = new DishService();

        // GET: Dishes
        public ActionResult Index()
        {
            return View(service.GetAllDishes());
        }

        // GET: Dishes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DishModel dishModel = service.GetDishById(id.Value);
            if (dishModel == null)
            {
                return HttpNotFound();
            }
            return View(dishModel);
        }

        // GET: Dishes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dishes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,DishType,WhenToServe,NumberOfGuests")] DishModel dishModel)
        {
            if (ModelState.IsValid)
            {
                service.AddDish(dishModel);
                return RedirectToAction("Index");
            }

            return View(dishModel);
        }

        // GET: Dishes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DishModel dishModel = service.GetDishById(id.Value);
            if (dishModel == null)
            {
                return HttpNotFound();
            }
            return View(dishModel);
        }

        // POST: Dishes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,DishType,WhenToServe,NumberOfGuests")] DishModel dishModel)
        {
            if (ModelState.IsValid)
            {
                service.EditDish(dishModel);
                return RedirectToAction("Index");
            }
            return View(dishModel);
        }

        // GET: Dishes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DishModel dishModel = service.GetDishById(id.Value);
            if (dishModel == null)
            {
                return HttpNotFound();
            }
            return View(dishModel);
        }

        // POST: Dishes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            service.DeleteDishById(id);
            return RedirectToAction("Index");
        }
    }
}
