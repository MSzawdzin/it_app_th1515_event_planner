﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EventPlanner.Logic.Services;
using EventPlanner.Models;
using EventPlanner.Models.Models;

namespace EventPlanner.Controllers
{
    public class ReservationsController : Controller
    {
        ReservationService service = new ReservationService();
        CateringService cateringService = new CateringService();

        // GET: ReservationModels
        public ActionResult Index()
        {
            return View(service.GetAllReservations());
        }

        // GET: ReservationModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReservationModel reservationModel = service.GetReservationById(id.Value);
            if (reservationModel == null)
            {
                return HttpNotFound();
            }
            return View(reservationModel);
        }

        // GET: ReservationModels/Create
        public ActionResult Create()
        {
            ViewBag.ID = new SelectList(cateringService.GetAllCaterings(), "ID", "ID");
            return View();
        }

        // POST: ReservationModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,When,Duration,NoOfGuests")] ReservationModel reservationModel)
        {
            if (ModelState.IsValid)
            {
                HallSettingService hallSettingService = new HallSettingService();
                var hsId = hallSettingService.AddHallSettingAndReturnId(new HallSettingModel());
               
                reservationModel.Catering = new CateringModel();
                reservationModel.HallSetting = hallSettingService.GetHallSettingById(hsId);
                                
                service.AddReservation(reservationModel);
                return RedirectToAction("Index");
            }

            ViewBag.ID = new SelectList(cateringService.GetAllCaterings(), "ID", "ID", reservationModel.ID);
            return View(reservationModel);
        }

        // GET: ReservationModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReservationModel reservationModel = service.GetReservationById(id.Value);
            if (reservationModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID = new SelectList(cateringService.GetAllCaterings(), "ID", "ID", reservationModel.ID);
            return View(reservationModel);
        }

        // POST: ReservationModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,When,Duration,NoOfGuests")] ReservationModel reservationModel)
        {
            if (ModelState.IsValid)
            {
                service.EditReservation(reservationModel);
                return RedirectToAction("Index");
            }
            ViewBag.ID = new SelectList(cateringService.GetAllCaterings(), "ID", "ID", reservationModel.ID);
            return View(reservationModel);
        }

        // GET: ReservationModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReservationModel reservationModel = service.GetReservationById(id.Value);
            if (reservationModel == null)
            {
                return HttpNotFound();
            }
            return View(reservationModel);
        }

        // POST: ReservationModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            service.DeleteReservationById(id);
            return RedirectToAction("Index");
        }
    }
}
