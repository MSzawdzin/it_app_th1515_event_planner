﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EventPlanner.Models;
using EventPlanner.Models.Models;

namespace EventPlanner.Controllers
{
    public class HallsController : Controller
    {
        Logic.Services.HallService service = new Logic.Services.HallService();

        // GET: Halls
        public ActionResult Index()
        {
            return View(service.GetAllHalls());
        }

        // GET: Halls/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HallModel hallModel = service.GetHallById(id.Value);
            if (hallModel == null)
            {
                return HttpNotFound();
            }
            return View(hallModel);
        }

        // GET: Halls/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Halls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Capacity,AdditionalInfo")] HallModel hallModel)
        {
            if (ModelState.IsValid)
            {
                service.AddHall(hallModel);
                return RedirectToAction("Index");
            }

            return View(hallModel);
        }

        // GET: Halls/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HallModel hallModel = service.GetHallById(id.Value);
            if (hallModel == null)
            {
                return HttpNotFound();
            }
            return View(hallModel);
        }

        // POST: Halls/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Capacity,AdditionalInfo")] HallModel hallModel)
        {
            if (ModelState.IsValid)
            {
                  service.EditHall(hallModel);
                  return RedirectToAction("Index");
            }
            return View(hallModel);
        }

        // GET: Halls/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var hallModel = service.GetHallById(id.Value);
            if (hallModel == null)
            {
                return HttpNotFound();
            }
            return View(hallModel);
        }

        // POST: Halls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            service.DeleteHallById(id);
            return RedirectToAction("Index");
        }
    }
}
