﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EventPlanner.Models;
using EventPlanner.Models.Models;
using EventPlanner.Logic.Services;

namespace EventPlanner.Controllers
{
    public class MealsController : Controller
    {
        private MealService service = new MealService();

        // GET: Meals
        public ActionResult Index()
        {
            return View(service.GetAllMeals());
        }

        // GET: Meals/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MealModel mealModel = service.GetMealById(id.Value);
            if (mealModel == null)
            {
                return HttpNotFound();
            }
            return View(mealModel);
        }

        // GET: Meals/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Meals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Prize,DishType")] MealModel mealModel)
        {
            if (ModelState.IsValid)
            {
                service.AddMeal(mealModel);
                return RedirectToAction("Index");
            }

            return View(mealModel);
        }

        // GET: Meals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MealModel mealModel = service.GetMealById(id.Value);
            if (mealModel == null)
            {
                return HttpNotFound();
            }
            return View(mealModel);
        }

        // POST: Meals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Prize,DishType")] MealModel mealModel)
        {
            if (ModelState.IsValid)
            {
                service.EditMeal(mealModel);
                return RedirectToAction("Index");
            }
            return View(mealModel);
        }

        // GET: Meals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MealModel mealModel = service.GetMealById(id.Value);
            if (mealModel == null)
            {
                return HttpNotFound();
            }
            return View(mealModel);
        }

        // POST: Meals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            service.DeleteMealById(id);
            return RedirectToAction("Index");
        }

    }
}
