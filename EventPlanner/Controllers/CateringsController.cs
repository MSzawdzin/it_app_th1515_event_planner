﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EventPlanner.Logic.Services;
using EventPlanner.Models;
using EventPlanner.Models.Models;

namespace EventPlanner.Controllers
{
    public class CateringsController : Controller
    {
        CateringService service = new CateringService();
        ReservationService reservationService = new ReservationService();
        // GET: Caterings
        public ActionResult Index()
        {
            return View(service.GetAllCaterings());
        }

        // GET: Caterings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CateringModel cateringModel = service.GetCateringById(id.Value);
            if (cateringModel == null)
            {
                return HttpNotFound();
            }
            return View(cateringModel);
        }

        // GET: Caterings/Create
        public ActionResult Create()
        {
            ViewBag.ID = new SelectList(reservationService.GetAllReservations(), "ID", "ID");
            return View();
        }

        // POST: Caterings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID")] CateringModel cateringModel)
        {
            if (ModelState.IsValid)
            {
                service.AddCatering(cateringModel);
                return RedirectToAction("Index");
            }

           ViewBag.ID = new SelectList(reservationService.GetAllReservations(), "ID", "ID", cateringModel.ID);
            return View(cateringModel);
        }

        // GET: Caterings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CateringModel cateringModel = service.GetCateringById(id.Value);
            if (cateringModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID = new SelectList(reservationService.GetAllReservations(), "ID", "ID", cateringModel.ID);
            return View(cateringModel);
        }

        // POST: Caterings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID")] CateringModel cateringModel)
        {
            if (ModelState.IsValid)
            {
                service.EditCatering(cateringModel);
                return RedirectToAction("Index");
            }
            ViewBag.ID = new SelectList(reservationService.GetAllReservations(), "ID", "ID", cateringModel.ID);
            return View(cateringModel);
        }

        // GET: Caterings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CateringModel cateringModel = service.GetCateringById(id.Value);
            if (cateringModel == null)
            {
                return HttpNotFound();
            }
            return View(cateringModel);
        }

        // POST: Caterings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            service.DeleteCateringById(id);
            return RedirectToAction("Index");
        }

    }
}
