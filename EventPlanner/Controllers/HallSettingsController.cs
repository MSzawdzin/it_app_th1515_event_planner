﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EventPlanner.Logic.Services;
using EventPlanner.Models;
using EventPlanner.Models.Models;

namespace EventPlanner.Controllers
{
    public class HallSettingsController : Controller
    {
        private HallSettingService service = new HallSettingService();

        // GET: HallSettings
        public ActionResult Index()
        {
            return View(service.GetAllHallSettings());
        }

        // GET: HallSettings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HallSettingModel hallSettingModel = service.GetHallSettingById(id.Value);
            if (hallSettingModel == null)
            {
                return HttpNotFound();
            }
            return View(hallSettingModel);
        }

        // GET: HallSettings/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HallSettings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,NumberOfSeats,TablesSetting")] HallSettingModel hallSettingModel)
        {
            if (ModelState.IsValid)
            {
                service.AddHallSetting(hallSettingModel);
                return RedirectToAction("Index");
            }

            return View(hallSettingModel);
        }

        // GET: HallSettings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HallSettingModel hallSettingModel = service.GetHallSettingById(id.Value);
            if (hallSettingModel == null)
            {
                return HttpNotFound();
            }
            return View(hallSettingModel);
        }

        // POST: HallSettings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,NumberOfSeats,TablesSetting")] HallSettingModel hallSettingModel)
        {
            if (ModelState.IsValid)
            {
                service.EditHallSetting(hallSettingModel);
                return RedirectToAction("Index");
            }
            return View(hallSettingModel);
        }

        // GET: HallSettings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HallSettingModel hallSettingModel = service.GetHallSettingById(id.Value);
            if (hallSettingModel == null)
            {
                return HttpNotFound();
            }
            return View(hallSettingModel);
        }

        // POST: HallSettings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            service.DeleteHallSettingById(id);
            return RedirectToAction("Index");
        }
    }
}
