﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace EventPlanner
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "ReservationDefault",
                "Reservations",
                new {controller="Reservations",action="Index"}
                );

            routes.MapRoute(
                "ClientDefault",
                "Clients",
                new { controller = "Clients", action = "Index" }
                );

            routes.MapRoute(
                "HallDefault",
                "Halls",
                new { controller = "Halls", action = "Index" }
                );

            routes.MapRoute(
                "DishDefault",
                "Dishes",
                new { controller = "Dishes", action = "Index" }
                );

            routes.MapRoute(
                "MealDefault",
                "Meals",
                new { controller = "Dishes", action = "Index" }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
