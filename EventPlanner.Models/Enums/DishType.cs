﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventPlanner.Models.Enums
{
    public enum DishType
    {
        Supper, Supper2, Dinner, HotDish, ColdDish, Dessert
    }
}
