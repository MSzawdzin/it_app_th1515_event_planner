﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventPlanner.Models.Enums
{
    public enum TablesSetting
    {
        UShapeSetting, TShapeSetting, WShapeSetting
    }
}
