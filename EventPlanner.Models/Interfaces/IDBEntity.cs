﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace EventPlanner.Models.Interfaces
{
    public interface IDBEntity
    {
        [Key]
        int ID { get; set; }
        bool IsDeleted { get; set; }
    }
}
