﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventPlanner.Models.Interfaces
{
    public interface IMappable<From>
    {
        void Map(From from, int level = 1);
    }
}
