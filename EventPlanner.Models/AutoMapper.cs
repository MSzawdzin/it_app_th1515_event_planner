﻿using AutoMapper;
using EventPlanner.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventPlanner.Models
{
    public class AutoMapper
    {
        public static TDestination Map<TSource, TDestination>(TSource source, int level = 1)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<TSource, TDestination>();
            });

            IMapper mapper = config.CreateMapper();
            var result = mapper.Map<TDestination>(source);

            if (level > 0 && result is IMappable<TSource>)
            {
                (result as IMappable<TSource>).Map(source, level - 1);
            }
            return result;
        }
    }
}
