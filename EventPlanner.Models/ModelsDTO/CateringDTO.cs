﻿using EventPlanner.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventPlanner.Models.ModelsDTO
{
    public class CateringDTO : IDBEntity
    {
        public bool IsDeleted { get; set; }
        public int ID { get; set; }
        public ReservationDTO Reservation { get; set; }

        public virtual ICollection<DishDTO> Dishes{ get; set; }
    }
}
