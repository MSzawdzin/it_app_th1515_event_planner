﻿using EventPlanner.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventPlanner.Models.ModelsDTO
{
    public class ReservationDTO : IDBEntity
    {
        public DateTime When { get; set; }
        public TimeSpan Duration { get; set; }
        public int NoOfGuests{ get; set; }
        public bool IsDeleted { get; set; }
        public int ID { get; set; }

        public virtual HallSettingDTO HallSetting { get; set; }
        public virtual HallDTO Hall { get; set; }

        public virtual CateringDTO Catering { get; set; }
        public virtual ClientDTO Client { get; set; }
        

    }
}
