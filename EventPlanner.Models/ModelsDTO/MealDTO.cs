﻿using EventPlanner.Models.Enums;
using EventPlanner.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventPlanner.Models.ModelsDTO

{
    public class MealDTO : IDBEntity
    {
        public string Name { get; set; }
        public double Prize { get; set; }
        public DishType DishType { get; set; }
        public bool IsDeleted { get; set; }
        public int ID { get; set; }

        public  ICollection<DishDTO> Dishes{ get; set; }
    }
}
