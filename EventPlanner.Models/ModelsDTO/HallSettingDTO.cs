﻿using EventPlanner.Models.Enums;
using EventPlanner.Models.Interfaces;
using System.Collections.Generic;
using EventPlanner.Models.Interfaces;

namespace EventPlanner.Models.ModelsDTO
{
    public class HallSettingDTO : IDBEntity
    {
        public int NumberOfSeats { get; set; }
        public TablesSetting TablesSetting{ get; set; }
        public bool IsDeleted { get; set; }
        public int ID { get; set; }

        public virtual ICollection<ReservationDTO> Reservations{ get; set; }


    }
}
