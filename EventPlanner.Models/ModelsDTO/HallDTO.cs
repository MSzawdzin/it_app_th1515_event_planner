﻿using EventPlanner.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventPlanner.Models.ModelsDTO
{
    public class HallDTO : IDBEntity
    {
        
        public int Capacity { get; set; }
        public string AdditionalInfo { get; set; }
        public bool IsDeleted { get; set; }
        public int ID { get; set; }


        public virtual ICollection<ReservationDTO> Reservations { get; set; }


    }
}
