﻿using EventPlanner.Models.Enums;
using EventPlanner.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventPlanner.Models.ModelsDTO
{
    public class DishDTO : IDBEntity
    {
        public DishType DishType { get; set; }
        public DateTime WhenToServe { get; set; }
        public int NumberOfGuests { get; set; }
        public bool IsDeleted { get; set; }
        public int ID { get; set; }
        



        public  CateringDTO Catering { get; set; }
        public  ICollection<MealDTO> Meals { get; set; }
    }
}
