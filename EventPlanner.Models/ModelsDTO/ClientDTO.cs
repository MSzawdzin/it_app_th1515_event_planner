﻿using EventPlanner.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventPlanner.Models.ModelsDTO
{
    public class ClientDTO : IDBEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsDeleted { get; set; }
        public int ID { get; set; }
        

        public virtual ICollection<ReservationDTO> Reservations { get; set; }
    }
}
