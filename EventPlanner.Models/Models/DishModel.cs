﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EventPlanner.Models.ModelsDTO;
using EventPlanner.Models.Interfaces;
using EventPlanner.Models.Enums;

namespace EventPlanner.Models.Models
{
    public class DishModel : IMappable<DishDTO>
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public DishType DishType { get; set; }
        [Required]
        public DateTime WhenToServe { get; set; }
        [Required]
        public int NumberOfGuests { get; set; }

        public CateringModel Catering { get; set; }
        public ICollection<MealModel> Meals { get; set; }

        public void Map(DishDTO from, int level = 1)
        {
            if (from.Catering != null)
                Catering = AutoMapper.Map<CateringDTO, CateringModel>(from.Catering);
            if (from.Meals != null)
                Meals = from.Meals.Select(x => AutoMapper.Map<MealDTO, MealModel>(x)).ToList();
        }
    }
}