﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EventPlanner.Models.ModelsDTO;
using EventPlanner.Models.Interfaces;

namespace EventPlanner.Models.Models
{
    public class ClientModel : IMappable<ClientDTO>
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required, EmailAddress]
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }

        public ICollection<ReservationModel> Reservations { get; set; }

        public void Map(ClientDTO from, int level = 1)
        {
            if (from.Reservations != null)
                Reservations = from.Reservations.Select(x => AutoMapper.Map<ReservationDTO, ReservationModel>(x)).ToList();
        }
    }
}