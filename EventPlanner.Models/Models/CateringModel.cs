﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EventPlanner.Models.ModelsDTO;
using EventPlanner.Models.Interfaces;

namespace EventPlanner.Models.Models
{
    public class CateringModel : IMappable<CateringDTO>
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public ReservationModel Reservation { get; set; }

        public List<DishModel> Dishes { get; set; }
        public void Map(CateringDTO from, int level = 1)
        {
            if (from.Dishes != null)
                Dishes = from.Dishes.Select(x => AutoMapper.Map<DishDTO, DishModel>(x)).ToList();
            if (from.Reservation != null)
                Reservation = AutoMapper.Map<ReservationDTO, ReservationModel>(from.Reservation);
        }
    }
}
