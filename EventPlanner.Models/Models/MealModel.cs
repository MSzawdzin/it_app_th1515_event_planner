﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EventPlanner.Models.ModelsDTO;
using EventPlanner.Models.Interfaces;
using EventPlanner.Models.Enums;

namespace EventPlanner.Models.Models
{
    public class MealModel : IMappable<MealDTO>
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public double Prize { get; set; }
        [Required]
        public DishType DishType { get; set; }

        public ICollection<DishModel> Dishes { get; set; }

        public void Map(MealDTO from, int level = 1)
        {
            if (from.Dishes != null)
                Dishes = from.Dishes.Select(x => AutoMapper.Map<DishDTO, DishModel>(x)).ToList();
        }
    }
}