﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EventPlanner.Models.ModelsDTO;
using EventPlanner.Models.Interfaces;

namespace EventPlanner.Models.Models
{
    public class ReservationModel : IMappable<ReservationDTO>
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public DateTime When { get; set; }

        [Required]
        public TimeSpan Duration { get; set; }

        [Required]
        public int NoOfGuests { get; set; }

        public  HallSettingModel HallSetting { get; set; }
        public  HallModel Hall { get; set; }
        public  CateringModel Catering { get; set; }
        public  ClientModel Client { get; set; }

        public void Map(ReservationDTO from, int level = 1)
        {
            if (from.HallSetting != null)
                HallSetting = AutoMapper.Map<HallSettingDTO, HallSettingModel>(from.HallSetting);
            if (from.Hall != null)
                Hall = AutoMapper.Map<HallDTO, HallModel>(from.Hall);
            if (from.Catering != null)
                Catering = AutoMapper.Map<CateringDTO, CateringModel>(from.Catering);
            if (from.Client != null)
                Client = AutoMapper.Map<ClientDTO, ClientModel>(from.Client);

        }
    }
}