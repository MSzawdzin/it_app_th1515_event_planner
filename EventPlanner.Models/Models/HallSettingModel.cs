﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EventPlanner.Models.ModelsDTO;
using EventPlanner.Models.Interfaces;
using EventPlanner.Models.Enums;

namespace EventPlanner.Models.Models
{
    public class HallSettingModel : IMappable<HallSettingDTO>
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public int NumberOfSeats { get; set; }
        [Required]
        public TablesSetting TablesSetting { get; set; }

        public ICollection<ReservationModel> Reservations { get; set; }

        public void Map(HallSettingDTO from, int level = 1)
        {
            if (from.Reservations != null)
                Reservations = from.Reservations.Select(x => AutoMapper.Map<ReservationDTO, ReservationModel>(x)).ToList();
        }
    }
}